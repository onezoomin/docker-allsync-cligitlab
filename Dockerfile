FROM registry.gitlab.com/onezoomin/docker-allsync-nodejs:latest

RUN apk add --no-cache py3-pip httpie
RUN pip install --break-system-packages apprise
RUN npm up && npm i -g @babel/runtime semantic-release @gitbeaker/cli 

COPY ./scripts/http-gl.sh /bin/http-gl

CMD ["/bin/bash"]
