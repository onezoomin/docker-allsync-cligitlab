# allsync-cligitlab
onezoomin/allsync-nodejs + gitbeaker

```
docker run --rm registry.gitlab.com/onezoomin/docker-allsync-cligitlab
```

### [GitBeaker docs](https://openbase.com/js/@gitbeaker/node/documentation)


#### Build
```shell {#build}
docker build -t allsync-cligitlab .
```
