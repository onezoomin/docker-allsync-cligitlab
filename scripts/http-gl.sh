#!/usr/bin/env bash
set -euxo pipefail

# Usage: http-gl PUT /projects/123/merge-requests/123/merge should_remove_source_branch==true
if [ $# -lt 2 ]; then
    echo "Error: Missing required arguments: METHOD URI"
    exit 1
fi

http --verify=no $1 "$GITLAB_HOST/api/v4$2" "Private-Token:$GITLAB_TOKEN" "${@:3}"
